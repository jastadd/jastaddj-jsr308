JastAddJ-JSR308
===============

This module is an extension to the JastAdd extensible Java compiler (JastAddJ), adding type annotations according to [JSR308](http://jcp.org/en/jsr/detail?id=308). JSR308 suggests extending Java with the possibility to add annotations in more locations. This module covers most of the JSR, but is not fully complete. In particular, annotations on multidimensional arrays are missing. Note also that the JSR is not finalized yet (as of Jan 1, 2014).

License and Copyright
---------------------

 * Copyright(c) 2005-2012, Torbj�rn Ekman
 * Copyright(c) JastAddJ-JSR308 Committers

All rights reserved.

JastAddJ-JSR308 is covered by the Modified BSD License. The full license text is distributed with this software. See the `LICENSE` file.

Building
--------

This module should be placed as a sibling directory of the JastAddJ directory (available at [jastadd.org](http://jastadd.org)). You only need to have javac and Apache Ant installed in order to build. All other tools used are available in JastAddJ.


Run ant to build the extended compiler `JavaJSR308Compiler.jar`:

    $ ant jar

Other build targets:

    $ ant clean   # Remove generated files

Running
-------

Usage:

    java -jar JavaJSR308Compiler.jar <options> <source files>
      -verbose                  Output messages about what the compiler is doing
      -classpath <path>         Specify where to find user class files
      -sourcepath <path>        Specify where to find input source files
      -bootclasspath <path>     Override location of bootstrap class files
      -extdirs <dirs>           Override location of installed extensions
      -d <directory>            Specify where to place generated class files
      -help                     Print a synopsis of standard options
      -version                  Print version information

Example:

    $ java -jar JavaJSR308Compiler.jar examples/DAG.java

